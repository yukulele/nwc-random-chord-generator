# NWC random chord generator

Random chord generator for [Noteworthy Composer](https://noteworthycomposer.com/)

🔗 <https://yukulele.gitlab.io/nwc-random-chord-generator/>

## Init project

```shell
make init
```

## Build

```shell
make
```

or

```shell
make watch
```

## Run

open `./index.html` in your browser
