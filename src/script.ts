type Chord = {
  name: string
  notes: Note[]
  bass: string
}
/**
 * - `''` none
 * - `'#'` sharp
 * - `'b'` flat
 * - `'n'` natural
 * - `'x'` double sharp
 * - `'v'` double flat
 */
type Accidental = 'x' | '#' | 'n' | 'b' | 'v' | ''

const noteRegex = /([x#nbv]?)\s*(-?[0-9]+)/
const arpeggioRegex = /^([123]+)(?:\s*\*\s*(\d+))?$/

class Note {
  root: number
  accidental: Accidental
  constructor(str: string) {
    str = str.trim()
    const match = noteRegex.exec(str)
    if (!match) throw new SyntaxError(str)
    this.root = Number(match[2])
    this.accidental = match[1] as Accidental
  }
  clone() {
    return new Note(this.toString())
  }
  toString() {
    return this.accidental + this.root
  }
}

const count = document.getElementById('count') as HTMLInputElement
const dice = document.getElementById('🎲') as HTMLElement
const avoidRepeat = document.getElementById('avoid-repeat') as HTMLInputElement
const repeat = document.getElementById('repeat') as HTMLInputElement
const times = document.getElementById('×') as HTMLSpanElement
const chordList = document.querySelector('#chords ul') as HTMLElement
const downloadBtn = document.getElementById('download') as HTMLElement
const clefSol = '|Clef|Type:Treble'
const clefFa = '|Clef|Type:Bass'
const timeSign = (sign = 'Common') => `|TimeSig|Signature:${sign}`

window.addEventListener('dblclick', e => e.preventDefault())

const duration: string[] = Array.prototype.slice.call({
  1: 'Whole',
  2: 'Half',
  3: '4th',
  4: '8th',
  5: '16th',
  6: '32nd',
  7: '64th',
  length: 8,
})

const parseArpeggio = (pattern = '1323*4') => {
  const match = pattern.match(arpeggioRegex)
  if (!match) throw new SyntaxError('wrong arppegio')
  const [_, pat, rep] = match
  pattern = pat.repeat(Number(rep ?? 1))
  return [...pattern].map(a => +a - 1) as (1 | 2 | 3)[]
}

const bar = '|Bar'
const note = (pos = '0', dur = 3) => `|Note|Dur:${duration[dur]}|Pos:${pos}`
const chord = '|Chord|Dur:Whole|Pos:'
const nuance = (n = 'f') => `|Dynamic|Style:${n}|Pos:-10`
const arpeggioPattern = parseArpeggio('1323')
const spaceRegex = /\s+/

const chords: Chord[] = `
C  -4,-2,1    -1
D♭ -3,b-1,b2   b0
D  #-3,-1,2    0
E♭ b-4,-2,b0   b1
E  -4,#-2,0    1
F  -3,-1,1     2
G♭ b-2,b0,b2   b-4
G  -2,0,2      -4
A♭ b-4,b-1,1   b-3
A  -4,-1,#1    -3
B♭ -3,b0,2     b-2
B  #-3,0,#2    -2
Cm  b-4,-2,1   -1
C♯m -4,#-2,#1  #-1
Dm  -3,-1,2    0
D♯m #-3,#-1,#2 #0
Em  -4,-2,0    1
Fm  -3,b-1,1   2
F♯m #-3,-1,#1  #2
Gm  -2,b0,2    -4
G♯m #-2,0,#2   #-4
Am  -4,-1,1    -3
A♯m #-4,#-1,#1 #-3
Bm  #-3,0,2    -2`
  .trim()
  .split(/\s*\n\s*/m)
  .map(line => {
    const a = line.split(spaceRegex)
    return {
      name: a[0],
      notes: a[1].split(',').map(s => new Note(s)),
      bass: a[2],
    }
  })

const intersperse = (arr: unknown[], val: unknown) => {
  return arr.reduce((acc: unknown[], next, i) => {
    if (i) acc.push(val)
    acc.push(next)
    return acc
  }, [])
}

const randomChord = () => {
  return chords[Math.floor(Math.random() * chords.length)]
}

const randomChords = (qte = 4, avoidRepeat = true) => {
  const chordList: Chord[] = []
  let previous: Chord | undefined
  let chord: Chord
  for (let i = 0; i < qte; i++) {
    while (true) {
      chord = randomChord()
      if (!avoidRepeat) break
      if (chord === previous) continue
      if (i === 0 || i < qte - 1 || chordList[i] !== chordList[0]) break
    }
    chordList.push(chord)
    previous = chord
  }
  return chordList
}

const toTxt = (song: Chord[]) => {
  return song.map(a => a.name).join(' ')
}

const fillList = (song: Chord[]) => {
  const frag = document.createDocumentFragment()
  for (const a of song) {
    const li = document.createElement('li')
    const btn = document.createElement('button')
    btn.textContent = a.name
    li.appendChild(btn)
    frag.appendChild(li)
  }
  chordList.innerHTML = ''
  chordList.appendChild(frag)
}

const arpeggioNwc = (chord: Chord) => {
  const pat = arpeggioPattern
    .map(a => {
      const div = Math.floor(a / 3)
      const mod = a - 3 * div
      const n = chord.notes[mod].clone()
      n.root += div * 7
      return note(n.toString(), 5)
    })
    .map(a => `${a}|Opts:Beam`)
  pat[0] += '=First'
  pat[pat.length - 1] += '=End'
  const ret: string[] = []
  for (let i = 0; i < 4; i++) ret.push(...pat)
  return ret
}

const toNwc = (song: Chord[], type = 'chord', repeat = 1) => {
  if (type !== 'chord' && type !== 'bass' && type !== 'arpeggio') {
    throw new Error('type error')
  }
  return [
    `|AddStaff|Name:"${type}"|Label:"${type}"`,
    `|StaffProperties|Muted:N|Volume:100|StereoPan:64|Device:0|Channel:${
      { chord: 1, bass: 2, arpeggio: 3 }[type]
    }`,
    `|StaffInstrument|Patch:${{ chord: 20, bass: 38, arpeggio: 46 }[type]}`,
    type === 'chord' && [
      '|Lyrics|Placement:Top|Align:Standard Rules|Offset:0',
      `|Lyric1|Text:"${toTxt(song)}"`,
    ],
    type === 'bass' ? clefFa : clefSol,
    timeSign(),
    repeat > 1 ? '|Bar|Style:LocalRepeatOpen' : null,
    nuance({ chord: 'p', bass: 'ff', arpeggio: 'mp' }[type]),
    ...intersperse(
      song.map(a => {
        if (type === 'chord') return chord + a.notes.join(',')
        if (type === 'bass') return note(a.bass, 1)
        if (type === 'arpeggio') return arpeggioNwc(a)
      }),
      bar,
    ),
    repeat > 1 ? `|Bar|Style:LocalRepeatClose|Repeat:${repeat}` : null,
  ]
    .flat(Number.POSITIVE_INFINITY)
    .filter(v => v)
}

const createNwcFile = (song: Chord[], repeat = 1) => {
  return [
    '!NoteWorthyComposer(2.751)',
    '|Font|Style:StaffLyric|Typeface:"DejaVu Sans"|Size:7.2|Bold:N|Italic:N|CharSet:0',
    ...toNwc(song, 'chord', repeat),
    ...toNwc(song, 'bass', repeat),
    ...toNwc(song, 'arpeggio', repeat),
    '!NoteWorthyComposer-End',
  ]
}

const download = (filename: string, mime: string, content: string) => {
  const element = document.createElement('a')
  element.setAttribute('href', `data:${mime},${encodeURIComponent(content)}`)
  element.setAttribute('download', filename)

  element.style.display = 'none'
  document.body.appendChild(element)

  element.click()

  document.body.removeChild(element)
}

const indexInParent = (elm: Node) => {
  for (let i = 0; ; i++) {
    const prev = elm.previousSibling
    if (!prev) return i
    elm = prev
  }
}

const generate = () => {
  song = randomChords(count.valueAsNumber, avoidRepeat.checked)
  fillList(song)
}

const changeRepeat = () => {
  times.textContent =
    repeat.valueAsNumber === 1 ? '' : ` × ${repeat.valueAsNumber}`
}

let song: Chord[]
count.addEventListener('input', generate)
dice.addEventListener('click', generate)
avoidRepeat.addEventListener('click', generate)
repeat.addEventListener('input', changeRepeat)
generate()
changeRepeat()

chordList.addEventListener('click', ({ target }) => {
  if (!(target instanceof HTMLButtonElement)) return
  const index = indexInParent(target.parentElement as HTMLElement)
  const oldChord = song[index]
  const prevChord = song[index > 0 ? index - 1 : song.length - 1]
  const nextChord = song[index + 1 < song.length ? index + 1 : 0]
  let chord: Chord
  while (true) {
    chord = randomChord()
    if (chord === oldChord) continue
    if (!avoidRepeat.checked) break
    if (song.length <= 1) break
    if (chord === prevChord) continue
    if (chord === nextChord) continue
    break
  }
  song[index] = chord
  fillList(song)
  ;(chordList.children[index].firstChild as HTMLButtonElement).focus()
})

downloadBtn.addEventListener('click', () =>
  download(
    'song.nwctxt',
    'application/nwctxt',
    createNwcFile(song, repeat.valueAsNumber).join('\n'),
  ),
)
