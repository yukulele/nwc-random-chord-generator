default: public/script.js

init: | node_modules

clean:
	if [ -d public ] ; \
		then rm -r public/ ; \
	fi

node_modules:
	npm install

public/script.js: public src/script.ts tsconfig.json | node_modules
	node_modules/.bin/bun run build

watch: public/script.js
	node_modules/.bin/bun run watch

public: src/index.html src/style.css
	mkdir -p public
	cp src/index.html src/style.css -t public


.PHONY: default init watch clean
